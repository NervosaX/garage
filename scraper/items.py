# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class GumtreeItem(scrapy.Item):
    url = scrapy.Field()
    title = scrapy.Field()
    address = scrapy.Field()
    suburb = scrapy.Field()
    details = scrapy.Field()
    date_start = scrapy.Field()
    date_end = scrapy.Field()
    longitude = scrapy.Field()
    latitude = scrapy.Field()
