import scrapy
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor

from scraper.items import GumtreeItem

class GumtreeSpider(CrawlSpider):
    name = "gumtree_spider"
    # allowed_domains = "http://www.gumtree.com.au"
    start_urls = [
        "http://www.gumtree.com.au/s-garage-sale/clayton-melbourne/c18486l3001603r10"
    ]

    def parse(self, response):
        for sel in response.xpath("//li[@itemprop='offers']"):
            url = sel.xpath("div//a[@itemprop='url']/@href").extract()
            if url:
                url = "http://www.gumtree.com.au{0}".format(url[0])
            title = sel.xpath("div//a/span[@itemprop]/text()").extract()
            if title:
                title = title[0]
            suburb = sel.xpath(
                "div//p[@class='rs-ad-location-suburb']/text()").extract()
            if suburb:
                suburb = suburb[0]
            dates = sel.xpath(
                "div//span[@class='rs-ad-attributes-event_mtdtcsv']/text()")
            date_start, date_end = ["", ""]
            if dates:
                dates = dates[0]
                date_start, date_end = dates.extract().strip("\r\n ") \
                    .replace(u"\xa0", "").split("to")

            data = {
                "url": url or "",
                "title": title or "",
                "suburb": suburb or "",
                "date_start": date_start or "",
                "date_end": date_end or ""
            }

            request = scrapy.Request(url, callback=self.detail_page_parse)
            request.meta["item"] = GumtreeItem(**data)
            yield request

        next_link = response.xpath(
            "//a[@class='rs-paginator-btn next']/@href").extract()

        # Follow "next" links
        if next_link:
            yield scrapy.Request(
                "http://www.gumtree.com.au{0}".format(next_link[0]),
                callback=self.parse)


    def detail_page_parse(self, response):
        item = response.meta["item"]

        address_sel = response.xpath(
            "//span[@class='j-google-map-link c-pointer']")
        latitude = address_sel.xpath("@data-lat").extract()

        if latitude:
            latitude = latitude[0]
        longitude = address_sel.xpath("@data-lng").extract()

        if longitude:
            longitude = longitude[0]

        address = address_sel.xpath("@data-address").extract()
        if address:
            address = address[0]

        details = "".join(response.xpath("//div[@id='ad-description']/text()") \
            .extract()).strip().replace("\r", ". ")

        item.update({
            "address": address or "",
            "latitude": latitude or "",
            "longitude": longitude or "",
            "details": details
        })

        return item
